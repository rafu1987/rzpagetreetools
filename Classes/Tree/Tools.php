<?php
namespace RZ\Rzpagetreetools\Tree;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Raphael Zschorsch <rafu1987@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Tree\Pagetree\Commands;

class Tools extends \TYPO3\CMS\Backend\Tree\Pagetree\Commands {
    
    public function hideShow($nodeData, $hideShow) {
        $node = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Tree\Pagetree\PagetreeNode', (array) $nodeData);
        $dataProvider = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Tree\Pagetree\DataProvider');

        try {
            $data['pages'][$node->getWorkspaceId()]['nav_hide'] = $hideShow;
            self::processTceCmdAndDataMap(array(), $data);
            
            $newNode = Commands::getNode($node->getId());
            $newNode->setLeaf($node->isLeafNode());
            $returnValue = $newNode->toArray();
        } catch (Exception $exception) {
            $returnValue = array(
                'success' => FALSE,
                'message' => $exception->getMessage(),
            );
        }

        return $returnValue;
    }

    public function switchPage($nodeData, $doktype) {
        $node = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Tree\Pagetree\PagetreeNode', (array) $nodeData);
        $dataProvider = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Tree\Pagetree\DataProvider');

        try {
            $data['pages'][$node->getWorkspaceId()]['doktype'] = $doktype;
            self::processTceCmdAndDataMap(array(), $data);
            
            $newNode = Commands::getNode($node->getId());
            $newNode->setLeaf($node->isLeafNode());
            $returnValue = $newNode->toArray();
        } catch (Exception $exception) {
            $returnValue = array(
                'success' => FALSE,
                'message' => $exception->getMessage(),
            );
        }

        return $returnValue;       
    }     

}